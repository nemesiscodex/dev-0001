package io.nemesiscodex.network.exceptions;

public class InvalidNetworkSizeException extends RuntimeException {
    public InvalidNetworkSizeException() {
        super("Invalid network size. The size of the network should be 0 or positive.");
    }
}

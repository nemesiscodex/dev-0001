package io.nemesiscodex.network.exceptions;

public class InvalidNodeException extends RuntimeException {
    public InvalidNodeException(String nodeName) {
        super("Index " + nodeName + " is out of range.");
    }
}

package io.nemesiscodex.network;

import io.nemesiscodex.network.exceptions.InvalidNetworkSizeException;
import io.nemesiscodex.network.exceptions.InvalidNodeException;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * Network class | Upwork | Julio Reyes
 */
public class Network {

    private int size;

    private Map<Integer, Set<Integer>> edges;

    /**
     * Creates new Network
     * @param size size of the network. size should be >= 0.
     * @throws InvalidNetworkSizeException if size < 0.
     */
    public Network(int size) {
        if(size < 0) {
            throw new InvalidNetworkSizeException();
        }
        this.size = size;
        edges = new HashMap<>();
    }

    /**
     * Creates a connection between nodeA and NodeB
     * @throws InvalidNodeException if node index is < 1 or > size
     */
    public void connect(int nodeA, int nodeB) {
        checkNode("nodeA", nodeA);
        checkNode("nodeB", nodeB);

        edges.computeIfAbsent(nodeA, n -> new HashSet<>())
            .add(nodeB);
        edges.computeIfAbsent(nodeB, n -> new HashSet<>())
            .add(nodeA);

    }

    /**
     * Checks if nodeA and nodeB are connected
     * @return true if connected, false otherwise. If nodeA == nodeB then it also returns true.
     * @throws InvalidNodeException if node index is < 1 or > size
     */
    public boolean query(int nodeA, int nodeB) {
        checkNode("nodeA", nodeA);
        checkNode("nodeB", nodeB);

        if(nodeA == nodeB) {
            return true;
        }

        Queue<Integer> q = new ArrayDeque<>();
        q.add(nodeA);

        boolean[] visited = new boolean[size];

        while(!q.isEmpty()) {
            int current = q.poll();
            visited[current-1] = true;

            Set<Integer> currentEdges = edges.getOrDefault(current, Collections.emptySet());
            if(currentEdges.contains(nodeB)) {
                return true;
            }

            for (Integer edge: currentEdges) {
                if(!visited[edge-1]) {
                    q.add(edge);
                }
            }

        }

        return false;
    }

    private void checkNode(String nodeName, int node) {
        if (node < 1 || node > size) {
            throw new InvalidNodeException(nodeName);
        }
    }

}

package io.nemesiscodex.network;

import io.nemesiscodex.network.exceptions.InvalidNetworkSizeException;
import io.nemesiscodex.network.exceptions.InvalidNodeException;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class NetworkTest {

    private Network exampleNetwork() {
        Network network = new Network(8);

        network.connect(1, 6);
        network.connect(1, 2);
        network.connect(2, 4);
        network.connect(2, 6);
        network.connect(5, 8);

        return network;
    }

    @Test
    public void queryExample() {
        Network network = exampleNetwork();

        Assert.assertTrue(network.query(1, 6));
        Assert.assertTrue(network.query(6, 1));

        Assert.assertTrue(network.query(1, 2));
        Assert.assertTrue(network.query(2, 1));

        Assert.assertFalse(network.query(7, 4));
        Assert.assertFalse(network.query(4, 7));

        // Identity
        Assert.assertTrue(network.query(1, 1));
        Assert.assertTrue(network.query(2, 2));
        Assert.assertTrue(network.query(8, 8));
    }

    @Test
    public void queryInvalid() {

        Network network = exampleNetwork();

        testException(() -> network.query(0, 1), InvalidNodeException.class);

        testException(() -> network.query(1, 0), InvalidNodeException.class);

        testException(() -> network.query(1, 9), InvalidNodeException.class);

        testException(() -> network.query(9, 6), InvalidNodeException.class);

        Network networkSingleNode = new Network(1);

        testException(() -> networkSingleNode.query(0, 1), InvalidNodeException.class);

        testException(() -> networkSingleNode.query(1, 0), InvalidNodeException.class);
    }

    @Test
    public void connectInvalid() {

        Network network = exampleNetwork();


        testException(() -> network.connect(0, 1), InvalidNodeException.class);

        testException(() -> network.connect(1, 0), InvalidNodeException.class);

        testException(() -> network.connect(1, 9), InvalidNodeException.class);

        testException(() -> network.connect(9, 6), InvalidNodeException.class);

        Network networkSingleNode = new Network(1);

        testException(() -> networkSingleNode.connect(0, 1), InvalidNodeException.class);

        testException(() -> networkSingleNode.connect(1, 0), InvalidNodeException.class);
    }


    @Test
    public void invalidNetworkSize() {
        testException(() -> new Network(-1), InvalidNetworkSizeException.class);
        testException(() -> new Network(-1), InvalidNetworkSizeException.class);
    }


    private <T extends RuntimeException> void testException(Runnable test, Class<T> exception) {
        try {
            test.run();
            fail("Should have thrown " + exception.getName());
        } catch (Exception ex) {
            if (!exception.isInstance(ex)) {
                fail("Should have thrown " + exception.getName());
            }
        }
    }

}
## Dev screening. Julio Reyes

#### Assumptions

- Network can have size 0, it just means it has no nodes.
- Node indexes start with 1.
- A node is connected to itself.
- The network graph is undirected.

#### Structure

- `src/main/java` -> source code
- `src/test/java` -> test source code

#### To run the tests:

Requires java 8

```bash
./gradlew clean test --info
```
Test results are available in: `./build/reports/tests/test/index.html`
